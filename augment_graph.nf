params.fasta = 'selected.fa'
params.vgs = 'graphs'
params.path_suffix = 'r0'

Channel.fromPath(params.fasta).splitFasta(record : [id : true, seqString : true] ).map
{
    record -> (chrom, start, end, rest) = record.id.split("_")
    return [ chrom, [start, end, record.seqString ]]
}.groupTuple(by: 0).set{chrom_ch}

Channel.fromPath("${params.vgs}/*.vg").map(){vg -> [vg.simpleName.substring(3), vg] }.set{vg_ch}

process augmentChrom {
    cpus '4'
    memory '40GB'
    time '72h'
    publishDir "augmented", mode: 'move'
//    scratch '$SLURM_TMPDIR'

    input:
    tuple val(chrom), val(loci), file(vg) from chrom_ch.join(vg_ch, by: 0)

    output:
    file "${chrom}.vg"

    script:
    seqs = ""
    id = 0
    for (locus in loci) {
        (start, end, seq) = locus
        seqs = seqs + "${start},${end},${id},${seq}\n"
        id = id + 1
    }
    """
    echo -n '${seqs}' > ${chrom}.txt
    cp -L ${vg} ${chrom}.vg
    while IFS=, read -r sub_st sub_end seq_num read
    do
    echo \$sub_st \$sub_end \$read >> done.txt
    vg chunk -p ${chrom}:\$sub_st-\$sub_end -x ${chrom}.vg -c 10 -t 4 > locus.vg
    vg index -G locus.gbwt -T locus.vg
    echo -n > mapping
    vg prune -M 4 -m mapping -a -u -g locus.gbwt locus.vg > pruned_locus.vg
    vg index -g locus.gcsa -f mapping pruned_locus.vg
    vg map -x locus.vg -g locus.gcsa -V ${chrom}_\${sub_st}_\${sub_end}_\${seq_num}_${params.path_suffix} -s \$read > locus.gam
    vg augment -i -s ${chrom}.vg locus.gam > ${chrom}_aug.vg
    mv ${chrom}_aug.vg ${chrom}.vg
    done < ${chrom}.txt
    """
}

