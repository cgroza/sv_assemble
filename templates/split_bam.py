#!/usr/bin/env python
import sys
import os
import pysam

def add_to_bucket(d, name, value):
    if name in d:
        d[name].add(value)
    else:
        d[name] = set([value])

# pysam.index("${sample_bam}")
# bamfile = pysam.AlignmentFile("${sample_bam}")
#pysam.index("NA12878_WGS_v2_phased_possorted_10p.bam")
#bamfile = pysam.AlignmentFile("NA12878_WGS_v2_phased_possorted_10p.bam")
bamfile = pysam.AlignmentFile(sys.argv[1])

#intervals_bed = open("NA12878_WGS_v2_large_sv_calls.bedpe")
#intervals_bed = open("${sample_svs}")
intervals_bed = open(sys.argv[2])
intervals = {}


# load all SV start sites
for line in intervals_bed:
    if line[0] == "#": continue
    fields = line.split()
    add_to_bucket(intervals, fields[0], int(fields[1]))

# SV interval associations with BX, MI tags
interval_MI = {}
interval_BX = {}

# Maximum distance from SV call to consider reads
MAX_DIST = 500
PRINT_STEP = 1000000

i = 0
# first pass collects relevant tag files
# skips unmapped files
for  read in bamfile.fetch(multiple_iterators = True):
    # progress report
    if i % PRINT_STEP == 0: print(i)
    i += 1

    if read.has_tag("BX") and read.reference_name in intervals:
            for i_start in intervals[read.reference_name]:
                if abs(i_start - read.reference_start) < MAX_DIST:
                    interval_name = read.reference_name + "_" + str(i_start)
                    # associate the BX tag with the interval
                    add_to_bucket(interval_BX, interval_name, read.get_tag("BX"))
                    # check if read has MI tag and associate it
                    if read.has_tag("MI"):
                        # associate the MI with the interval
                        add_to_bucket(interval_MI, interval_name, read.get_tag("MI"))

# Print summaries
print("BXs for each interval:")
for i in interval_BX: print(i, len(interval_BX[i]))
print(interval_BX)
print("MIs for each interval:")
for i in interval_MI: print(i, len(interval_MI[i]))
print(interval_MI)


def in_buckets(d, value):
    names = []
    for name in d:
        if value in d[name]:
            names.append(name)
    return names

# open file handles
os.makedirs("split", exist_ok = True)
file_handles = {}
for interval_name in interval_MI:
    file_handles[interval_name] = pysam.AlignmentFile(os.path.join("split", interval_name + ".bam"), "wb", template = bamfile)

# this time, we do not skip unmapped files
i = 0
for read in bamfile.fetch(multiple_iterators = True, until_eof = True):
    # progress report
    if i % PRINT_STEP == 0: print(i)
    i += 1
    # retrieve if its MI is associated with an interval call
    if read.has_tag("MI"):
        MI_assoc_intervals = in_buckets(interval_MI, read.get_tag("MI"))
        for assoc_interval in MI_assoc_intervals:
            file_handles[assoc_interval].write(read)

    elif read.is_unmapped and read.has_tag("BX"):
        BX_assoc_intervals = in_buckets(interval_BX, read.get_tag("BX"))
        for assoc_interval in BX_assoc_intervals:
            file_handles[assoc_interval].write(read)

for interval_name in file_handles:
    file_handles[interval_name].close()
