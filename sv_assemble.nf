params.sampleset = 'samples.csv'
params.genome = 'hg19.genome'
params.genotype = 1
params.reference = 'hg19.fa'
params.cores = 40
params.sv_window = 400
params.min_overlap = 30
params.extra = "-S -P"
params.publishName = ""
params.vcf_header = "header.vcf"
params.min_insert = 50

Channel.fromPath(params.sampleset).splitCsv(header:true).map{row ->
    tuple(row.name, file(row.vcf), file(row.bam), file(row.bam + ".bai"), file(row.bx_bam), file(row.bx_bam + ".bai"))}.view().set{sample_ch}

process bxlra_sample {
    publishDir "${workflow.launchDir}/${params.publishName}"
    cpus "${params.cores}"
    memory '80 GB'
    time '7h'
    module "bedtools:samtools:vcftools:tabix:bcftools:python/3.8.2"

    input:
    set val(name), file(vcf), file(bam), file(bam_index), file(bx_bam), file(bx_bam_index) from sample_ch

    output:
    file name into vcf_ch
//    file "${name}/${name}.vcf.gz" into vcf_ch

    script:
    """
module load samtools bedtools vcftools bcftools python/3.8.2 tabix
cat ${vcf} | grep -v _gl | bcftools view -c ${params.genotype} | vcf2bed | cut -f1-3 | sed 's/chr//g' > ${name}_calls.bed

nlines=\$(cat ${name}_calls.bed | wc -l)
if [ \$nlines -eq 0 ]
then
    exit 0
fi

bedtools slop -b ${params.sv_window} -i ${name}_calls.bed -g ${params.genome} > ${name}.bed

mkdir ${name}

cd ${name}

timeout -k 6.1h 6h ~/bxlra/bin/bxlra -t ${params.cores} -b ../${bam} -B ../${bx_bam} -r ../${name}.bed -g ${params.reference} -o ${params.min_overlap} ${params.extra} -F /home/cgroza/scratch/sv_assemble/Human_TE_library.fa || :

python3 ~/bxlra/scripts/alignment_to_vcf.py --sample ${name} --alns alignments.tsv --ref ${params.reference} --contigs contigs.fa --vcf_template ${params.vcf_header} --vcf_out ${name}.vcf --out_contigs selected_${name}.fa --min_insert ${params.min_insert}

bcftools sort ${name}.vcf | bgzip > ${name}.vcf.gz
cd ..
"""
}

process merge_vcfs {
    publishDir "${workflow.launchDir}/${params.publishName}"
    cpus '1'
    memory '2GB'
    time '1h'
    module "tabix:bcftools"

    input:
    file "*" from vcf_ch.collect()

    output:
    file "merged.vcf.gz"

    script:
"""
module load bcftools tabix
ls */*.vcf.gz | parallel tabix {}
bcftools merge */*.vcf.gz | bgzip > merged.vcf.gz
"""
}

